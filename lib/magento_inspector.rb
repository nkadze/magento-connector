class MagentoInspector
  require 'oauth'
  require 'omniauth'
  require 'multi_json'

  MAGENTO_URL = 'http://magsan.yardbook.com'

  CONSUMER_KEY = '34e3747194003185e4f30bc0ffab26f6'
  CONSUMER_SECRET = 'aaf17e48716e81f46d49a797b8be90b8'

  ACCESS_TOKEN = 'ab884e5d213b9aaeeca3ff93cba3fa1d'
  ACCESS_SECRET = '1602ccbe02cc1df2b31e5a8fd4f1156a'

  def connector(url, header)
    MultiJson.decode(prepare_access_token.get(url, header).body)
  end

  private

  def prepare_access_token
    consumer = OAuth::Consumer.new(CONSUMER_KEY, CONSUMER_SECRET, {site: MAGENTO_URL})
    token_hash = {oauth_token: ACCESS_TOKEN, oauth_token_secret: ACCESS_SECRET}
    OAuth::AccessToken.from_hash(consumer, token_hash)
  end
end