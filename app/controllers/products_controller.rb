class ProductsController < ApplicationController

  def index
    magento = MagentoInspector.new
    @response = magento.connector('/api/rest/products', { 'Content-Type'=>'application/json', 'Accept' => 'application/json' })
  end
end