# README

Conncetor Code in /lib/magento_inspector.rb

Patch to ignore ssl verification in config/initializers/opensslpatch.rb


[Magento REST API documentation](http://devdocs.magento.com/guides/m1x/api/rest/Resources/resources.html)


Magento Consumer Setup
-----------------------

1)
Log in Magento as admin
System -> web services -> REST OAuth Consumers
add new consumer

2)
in Postman
GET http://magsan.yardbook.com/oauth/initiate
with OAuth 1.0
parameters:
	oauth_callback: https://httpbin.org/get
fill consumer key and secret with values from Magento System -> web services -> REST OAuth Consumers

![First Step in Postman]
(https://snag.gy/R4SqPb.jpg)
	

That will give you an oauth_token and oauth_token_secret, which are only temporary. 
These are referred to as a "request token" and secret. Save these values somewhere because you will need them later.

3)
open your browser and hit url:
https://magsan.yardbook.com/admin/oauth_authorize?oauth_token=returned_token&oauth_token_secret=returned_secret

after confirm your authentication by Magento admin user and password

it will be redirected to httpbin url, which will contain oauth response

4)
in Postman
Use the oauth_token, oauth_secret, and oauth_verifier all together to get a valid and persistent "access token". 
Take the oauth_token_secret from the first step, and combine and assemble a new OAuth request.

GET http://magsan.yardbook.com/oauth/token
parameters: 
	oauth_verifier: returned_oauth_verifier
fill consumer key and secret with values from Magento System -> web services -> REST OAuth Consumers	
also check the "Add params to header" checkbox in order for Magento REST calls to work properly

![Second Step in Postman]
(https://snag.gy/a1C5jX.jpg)

You should get a returned token and secret. These will never expire
To use them in Rails based Magento Connector change values of constant variables in /lib/magento_inspector.rb

5)
Log in Magento as admin
check new entries in
	System -> web services -> REST My apps
	System -> web services -> REST Oauth authorized tokens

check if you have admin role in System -> web services -> REST Roles, if not add admin role with all privilegies
go in System -> Permissions -> Users check your admin user, in REST Roles section check radio button for Admin (REST Role) and save user

NGINX Magento REST Setup
----------------

to make nginx respond REST route calls

add section in nginx configuration file under your magento app server section, 
according to current configuration file path is /etc/nginx/conf.d/magsan.yardbook.com.conf
```
location /api {
  rewrite ^/api/rest /api.php?type=rest last;
}
```